using System;
using Totvs.Application.Services;
using Xunit;

namespace Totvs.Tests.UnitTests
{
    public class SecurityServiceUnitTest
    {
        [Theory]
        [InlineData("2", true)]
        public void SecurityService_GenerateJwtTokenCorrectly(string id, bool expected)
        {
            bool actual = !String.IsNullOrEmpty(SecurityService.GenerateJwtToken(id));

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImEyZTI0ZDg1LWE3N2YtNGZjMi1iZTU3LThjNDU1ZWQwMTIzOCIsIm5iZiI6MTU5ODg3NTEwMiwiZXhwIjoxNTk4ODc2OTAyLCJpYXQiOjE1OTg4NzUxMDJ9.hjr3PuGrYHiX6z3UB5pg4qmXCLIJQ9IUbXp3Uxca2g4", "a2e24d85-a77f-4fc2-be57-8c455ed01238")]
        public void SecurityService_ValidateJwtToken(string token, string expected)
        {
            string actual = SecurityService.ValidateJwtToken(token);

            Assert.Equal(expected, actual);
        }
    }
}
