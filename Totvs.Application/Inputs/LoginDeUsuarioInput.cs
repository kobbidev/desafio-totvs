﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Totvs.Application.Inputs
{
    public class LoginDeUsuarioInput
    {
        public string email { get; set; }
        public string senha { get; set; }
    }
}
