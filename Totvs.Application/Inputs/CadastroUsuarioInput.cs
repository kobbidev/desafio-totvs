﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Totvs.Core.Domain;

namespace Totvs.Application.Inputs
{
    public class CadastroUsuarioInput
    {
        public string name { get; set; }
        public string email { get; set; }
        public ICollection<CadastroUsuarioTelefoneInput> Phones { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }

    public class CadastroUsuarioTelefoneInput
    {
        public string ddd { get; set; }
        public string number { get; set; }
    }
}
