﻿using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Application.Inputs;
using Totvs.Application.Outputs;
using Totvs.Application.Services;
using Totvs.Core.Domain;
using Totvs.Infrastructure.Database;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.IdentityModel.Tokens.Jwt;

namespace Totvs.Application.UseCases
{
    public class UsuariosUseCases : IUsuariosUseCases
    {
        private readonly ICQRS _cqrs;
        private readonly SignInManager<Usuario> _signInManager;

        public UsuariosUseCases(ICQRS cqrsService, SignInManager<Usuario> signInManager)
        {
            _cqrs = cqrsService;
            _signInManager = signInManager;
        }

        public CadastroUsuarioOutput CadastrarUsuario(CadastroUsuarioInput input)
        {
            Tuple<Usuario, string> resultado = ValidarInput(input);
            if (resultado?.Item1 != null)
            {
                //* Sqlite não está gerando o código na inserção, mesmo tendo especificado no model builder e no atributo que a coluna Codigo é Identity
                //* O uso do método abaixo não é recomendado por dois motivos: a busca traz todos os cadastros para a memória e pode haver problemas de concorrência durante o cadastro
                resultado.Item1.Codigo = _cqrs.Query.Usuarios.BuscarUsuarios().Count() + 1;
                var createResult = _signInManager.UserManager.CreateAsync(resultado.Item1, input.Password);
                if (createResult.Result.Succeeded)
                {
                    // guardar token
                    var usuarioCadastrado = _cqrs.Query.Usuarios.BuscarUsuarioPorEmail(resultado.Item1.Email);
                    if(usuarioCadastrado != null)
                    {
                        usuarioCadastrado.Token = SecurityService.GenerateJwtToken(resultado.Item1.Id);
                        _cqrs.Command.SaveChanges();
                        return new CadastroUsuarioOutput(usuarioCadastrado, Output.StatusOutput.Criado, "Cadastro realizado com sucesso");
                    }
                }
                
                return new CadastroUsuarioOutput(null, Output.StatusOutput.ErroInterno, $"Falha ao realizar o cadastro -> {createResult.Result}");
            }
            else
            {
                return new CadastroUsuarioOutput(Output.StatusOutput.InputInvalido, resultado.Item2);
            }
        }

        public PerfilUsuarioOutput ConsultarUsuario(int input, string token) 
        {
            Tuple<Usuario, Output.StatusOutput, string> resultado = ValidarInput(input, token);
            if (resultado?.Item1 != null)
            {
                return new PerfilUsuarioOutput(resultado.Item1, resultado.Item2, resultado.Item3);
            }
            else
            {
                return new PerfilUsuarioOutput(resultado.Item2, resultado.Item3);
            }
        }

        public PerfilUsuarioOutput Login(LoginDeUsuarioInput input)
        {
            Tuple<Usuario, string> resultado = ValidarInput(input);
            if (resultado?.Item1 != null)
            {
                resultado.Item1.Token = SecurityService.GenerateJwtToken(resultado.Item1.Id);
                _cqrs.Command.SaveChanges();
                return new PerfilUsuarioOutput(resultado.Item1, Output.StatusOutput.Sucesso, resultado.Item2);
            }
            else
            {
                return new PerfilUsuarioOutput(Output.StatusOutput.InputInvalido, resultado.Item2);
            }
        }

        private Tuple<Usuario, string> ValidarInput(CadastroUsuarioInput input)
        {
            var usuarioJaCadastrado = _cqrs.Query.Usuarios.BuscarUsuarioPorEmail(input.email);
            if (usuarioJaCadastrado != null)
            {
                return Tuple.Create<Usuario, string>(null, "E-mail já cadastrado");
            }
            else
            {
                var usuario = new Usuario
                {
                    name = input.name,
                    Email = input.email,
                    UserName = input.email,
                    Phones = input.Phones.Select(p => new Telefone{ ddd = p.ddd, number = p.number }).ToList(),
                    CriadoEm = DateTime.Now
                };

                return Tuple.Create<Usuario, string>(usuario, "Cadastro válido!");
            }
        }

        private Tuple<Usuario, Output.StatusOutput, string> ValidarInput(int input, string token)
        {
            var usuarioCadastrado = _cqrs.Query.Usuarios.BuscarUsuarioPorId(input);
            if (usuarioCadastrado == null)
            {
                return Tuple.Create<Usuario, Output.StatusOutput, string>(null, Output.StatusOutput.NaoEncontrado, "Não encontrado");
            }
            else if (usuarioCadastrado.Token != token)
            {
                return Tuple.Create<Usuario, Output.StatusOutput, string>(null, Output.StatusOutput.NaoAutorizado, "Não autorizado");
            }
            else if (usuarioCadastrado.LastLogin.HasValue && usuarioCadastrado.LastLogin.Value.AddMinutes(30) > DateTime.Now)
            {
                return Tuple.Create<Usuario, Output.StatusOutput, string>(null, Output.StatusOutput.SessaoInvalida, "Sessão inválida");
            }
            else
            {
                return Tuple.Create<Usuario, Output.StatusOutput, string>(usuarioCadastrado, Output.StatusOutput.Sucesso, "Perfil encontrado!");                
            }
        }

        private Tuple<Usuario, string> ValidarInput(LoginDeUsuarioInput input)
        {
            //var usuarioCadastrado = _cqrs.Query.Usuarios.BuscarUsuarioPorEmail(input.email);

            var usuarioCadastrado = _cqrs.Command.Usuarios.Where(x => x.Email == input.email).FirstOrDefault();
            if (usuarioCadastrado == null)
            {
                return Tuple.Create<Usuario, string>(null, "Usuário e/ ou senha inválidos");
            }
            else
            {
                var checkResult = _signInManager.CheckPasswordSignInAsync(usuarioCadastrado, input.senha, false);
                if (!checkResult.Result.Succeeded)
                {
                    return Tuple.Create<Usuario, string>(null, "Usuário e/ ou senha inválidos");
                }
                else
                {
                    return Tuple.Create<Usuario, string>(usuarioCadastrado, "Login realizado com sucesso!");
                }
            }
        }
    }
}
