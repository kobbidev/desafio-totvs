﻿using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Application.Inputs;
using Totvs.Application.Outputs;

namespace Totvs.Application.UseCases
{
    public interface IUsuariosUseCases
    {
        CadastroUsuarioOutput CadastrarUsuario(CadastroUsuarioInput input);
        PerfilUsuarioOutput ConsultarUsuario(int input, string token);
        PerfilUsuarioOutput Login(LoginDeUsuarioInput input);
    }
}
