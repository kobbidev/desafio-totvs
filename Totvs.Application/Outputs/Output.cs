﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Totvs.Application.Outputs
{
    public class Output : IOutput
    {
        public Output(StatusOutput status, string mensagem)
        {
            this.Status = status;
            this.Mensagem = mensagem;
        }

        public StatusOutput Status { get; private set; }
        public string Mensagem { get; private set; }
        public enum StatusOutput
        {
            Sucesso,
            Criado,
            InputInvalido,
            NaoEncontrado,
            NaoAutorizado,
            SessaoInvalida,
            ErroInterno
        }
    }
}
