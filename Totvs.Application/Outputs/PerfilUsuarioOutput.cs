﻿using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Application.Services;
using Totvs.Core.Domain;

namespace Totvs.Application.Outputs
{
    public class PerfilUsuarioOutput : Output
    {
        public PerfilUsuarioOutput(StatusOutput status, string mensagem) : base(status, mensagem)
        {
        }

        public PerfilUsuarioOutput(Usuario usuario, StatusOutput status, string mensagem) : base(status, mensagem)
        {
            if (usuario != null)
            {
                this.name = usuario.name;
                this.email = usuario.Email;
                this.Phones = usuario.Phones;
                this.Token = usuario.Token;
            }
        }

        public string name { get; set; }
        public string email { get; set; }
        public ICollection<Telefone> Phones { get; set; }
        public string Token { get; set; }
    }
}
