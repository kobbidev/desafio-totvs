﻿using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Core.Domain;
using static Totvs.Application.Outputs.Output;

namespace Totvs.Application.Outputs
{
    public class CadastroUsuarioOutput : Output, IOutput
    {
        public CadastroUsuarioOutput(StatusOutput status, string mensagem) : base(status, mensagem)
        {

        }

        public CadastroUsuarioOutput(Usuario usuario, StatusOutput status, string mensagem) : base(status, mensagem)
        {
            if(usuario != null)
            {
                this.Id = usuario.Id;
                this.name = usuario.name;
                this.email = usuario.Email;
                this.Phones = usuario.Phones;
                this.LastLogin = usuario.LastLogin;
                this.Token = usuario.Token;
                this.CreatedAt = usuario.CriadoEm;
                this.UpdatedAt = usuario.CriadoEm;
            }
        }

        public string Id { get; private set; }
        public string name { get; private set; }
        public string email { get; private set; }
        public ICollection<Telefone> Phones { get; private set; }
        public DateTime? LastLogin { get; private set; }
        public string Token { get; private set; }
        public DateTime? CreatedAt { get; private set; }
        public DateTime? UpdatedAt { get; private set; }
    }
}
