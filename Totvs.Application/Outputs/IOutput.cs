﻿using System;
using System.Collections.Generic;
using System.Text;
using static Totvs.Application.Outputs.Output;

namespace Totvs.Application.Outputs
{
    public interface IOutput
    {
        StatusOutput Status { get; }
        string Mensagem { get; }
    }
}
