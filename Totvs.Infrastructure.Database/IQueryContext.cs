﻿using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Infrastructure.Database.Repositories;

namespace Totvs.Infrastructure.Database
{
    public interface IQueryContext
    {
        IUsuarioQuery Usuarios { get; set; }
    }
}
