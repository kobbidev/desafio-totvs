﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Totvs.Infrastructure.Database
{
    public interface ICQRS
    {
        IDataContext Command { get; }
        IQueryContext Query { get; }
    }
}
