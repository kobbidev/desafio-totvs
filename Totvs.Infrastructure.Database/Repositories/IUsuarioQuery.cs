﻿using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Core.Domain;

namespace Totvs.Infrastructure.Database.Repositories
{
    public interface IUsuarioQuery
    {
        IEnumerable<Usuario> BuscarUsuarios();
        Usuario BuscarUsuarioPorId(int id);
        Usuario BuscarUsuarioPorEmail(string email);
    }
}
