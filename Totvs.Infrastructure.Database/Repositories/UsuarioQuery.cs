﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using Totvs.Core.Domain;

namespace Totvs.Infrastructure.Database.Repositories
{
    public class UsuarioQuery : IUsuarioQuery
    {
        string _connectionString;
        public UsuarioQuery(string connString)
        {
            this._connectionString = connString;
        }

        public Usuario BuscarUsuarioPorEmail(string email)
        {
            Usuario usuario = new Usuario();
            using (var database = new SqliteConnection(_connectionString))
            {
                try
                {
                    var query = "SELECT * FROM Usuarios WHERE Email LIKE '" + email + "'";
                    usuario = database.QueryFirstOrDefault<Usuario>(query);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    database.Close();
                }
                return usuario;
            }
        }

        public Usuario BuscarUsuarioPorId(int id)
        {
            Usuario usuario = new Usuario();
            using (var database = new SqliteConnection(_connectionString))
            {
                try
                {
                    database.Open();
                    var query = "SELECT * FROM Usuarios WHERE Codigo = " + id;
                    usuario = database.QueryFirstOrDefault<Usuario>(query);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    database.Close();
                }
                return usuario;
            }
        }
        public IEnumerable<Usuario> BuscarUsuarios()
        {
            IEnumerable<Usuario> usuarios = new List<Usuario>();
            using (var database = new SqliteConnection(_connectionString))
            {
                try
                {
                    database.Open();
                    var query = "SELECT * FROM Usuarios";
                    usuarios = database.Query<Usuario>(query).AsList<Usuario>();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    database.Close();
                }
                return usuarios;
            }
        }
    }
}
