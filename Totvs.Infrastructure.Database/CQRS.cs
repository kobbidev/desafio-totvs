﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Totvs.Infrastructure.Database
{    
    public class CQRS : ICQRS
    {
        public CQRS(IDataContext dataContext, IQueryContext queryContext)
        {
            this.Command = dataContext;
            this.Query = queryContext;
        }
        public IDataContext Command { get; private set; }
        public IQueryContext Query { get; private set; }
    }
}
