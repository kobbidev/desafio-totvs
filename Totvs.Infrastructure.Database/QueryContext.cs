﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Infrastructure.Database.Repositories;

namespace Totvs.Infrastructure.Database
{    public class QueryContext : IQueryContext
    {
        public QueryContext(DataContext context)
        {
            this.Usuarios = new UsuarioQuery(context.GetConnectionString());
        }

        public IUsuarioQuery Usuarios { get; set; }
    }
}
