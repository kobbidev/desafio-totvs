﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Core.Domain;
using Totvs.Infrastructure.Database.Repositories;

namespace Totvs.Infrastructure.Database
{
    public interface IDataContext
    {
        DbSet<Usuario> Usuarios { get; set; }
        int SaveChanges();
    }
}
