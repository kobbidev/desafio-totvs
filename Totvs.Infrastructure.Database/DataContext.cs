﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Totvs.Core.Domain;
using Totvs.Infrastructure.Database.Repositories;

namespace Totvs.Infrastructure.Database
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) 
        {
            
        }

        public void Migrate() => base.Database.Migrate();
        public string GetConnectionString() => base.Database.GetDbConnection().ConnectionString;
        public override int SaveChanges() => base.SaveChanges();

        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>()
                .ToTable("Usuarios")
                .HasKey(x => x.Id);

            modelBuilder.Entity<Usuario>()
                .Property<int>(u => u.Codigo)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Telefone>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Telefone>()
                .HasOne(x => x.Usuario)
                .WithMany()
                .HasForeignKey(x => x.UsuarioId);
        }

    }
}
