using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Totvs.Core.Domain;
using Totvs.Infrastructure.Database;
using AutoMapper;
using Totvs.Presentation.API.Profiles;
using System;

namespace Totvs.Presentation.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<DataContext>(opt =>
            {
                // *N�o consegui conectar o Dapper ao InMemoryDatabase
                // *Tamb�m tentei utilizar o Sqlite no modo InMemory, mas o migration n�o estava sendo executado, mesmo aumentando o lifetime para singleton
                // opt.UseInMemoryDatabase("totvs");
                //opt.UseSqlite("Data Source=:memory:");
                //opt.UseSqlServer(Configuration.GetConnectionString("TotvsDatabase"));
                var filePath = Environment.CurrentDirectory + Configuration.GetConnectionString("TotvsFileDatabase");
                opt.UseSqlite("Data Source = " + filePath);
            }, ServiceLifetime.Singleton);

            services.AddIdentity<Usuario, IdentityRole>()
               .AddEntityFrameworkStores<DataContext>();

            services.AddScoped<IQueryContext, QueryContext>();

            services.AddAutoMapper(typeof(Startup));

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DataContext>();
                if (!context.Database.EnsureCreated())
                    context.Database.Migrate();

                /* Sqlite InMemory */
                /*
                 * var context = serviceScope.ServiceProvider.GetService<DataContext>();
                 * if (!context.Database.EnsureCreated())
                 *   context.Database.Open();
                 *   context.Database.Migrate();
                */
            }
        }
    }
}
