﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Totvs.Application.Outputs;

namespace Totvs.Presentation.API.Services
{
    public static class ActionResultFactory
    {
        public static IActionResult CreateResult(this ControllerBase result, IOutput output, Object response)
        {
            switch (output.Status)
            {
                case Output.StatusOutput.Sucesso:
                case Output.StatusOutput.Criado:
                    return result.Ok(response);
                case Output.StatusOutput.InputInvalido:
                case Output.StatusOutput.SessaoInvalida:
                    return result.BadRequest(MensagemDeErroFactory.Create(output.Mensagem));
                case Output.StatusOutput.NaoEncontrado:
                    return result.NotFound(MensagemDeErroFactory.Create(output.Mensagem));
                case Output.StatusOutput.NaoAutorizado:
                    return result.Unauthorized(MensagemDeErroFactory.Create(output.Mensagem));
                case Output.StatusOutput.ErroInterno:
                default:
                    return result.StatusCode(500, MensagemDeErroFactory.Create(output.Mensagem));
            }
        }
    }
}
