﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Totvs.Presentation.API.ViewModels;

namespace Totvs.Presentation.API.Services
{
    public static class MensagemDeErroFactory
    {
        public static MensagemDeErro Create(string message)
        {
            return new MensagemDeErro(message);
        }
    }
}
