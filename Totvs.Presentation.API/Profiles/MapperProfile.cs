﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Totvs.Application.Outputs;
using Totvs.Presentation.API.ViewModels;

namespace Totvs.Presentation.API.Profiles
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CadastroUsuarioOutput, CadastroViewModel>()
            .ForMember(dst => dst.Id, map => map.MapFrom(src => src.Id))
            .ForMember(dst => dst.name, map => map.MapFrom(src => src.name))
            .ForMember(dst => dst.email, map => map.MapFrom(src => src.email))
            .ForMember(dst => dst.phones, map => map.MapFrom(src => src.Phones))
            .ForMember(dst => dst.created, map => map.MapFrom(src => src.CreatedAt.Value.ToString()))
            .ForMember(dst => dst.modified, map => map.MapFrom(src => src.UpdatedAt.Value.ToString()))
            .ForMember(dst => dst.last_login, map => map.MapFrom(src => src.LastLogin))
            .ForMember(dst => dst.token, map => map.MapFrom(src => src.Token));

            CreateMap<PerfilUsuarioOutput, PerfilViewModel>()
            .ForMember(dst => dst.name, map => map.MapFrom(src => src.name))
            .ForMember(dst => dst.email, map => map.MapFrom(src => src.email))
            .ForMember(dst => dst.phones, map => map.MapFrom(src => src.Phones));

            CreateMap<PerfilUsuarioOutput, LoginViewModel>()
            .ForMember(dst => dst.name, map => map.MapFrom(src => src.name))
            .ForMember(dst => dst.email, map => map.MapFrom(src => src.email))
            .ForMember(dst => dst.token, map => map.MapFrom(src => src.Token))
            .ForMember(dst => dst.phones, map => map.MapFrom(src => src.Phones));
        }
    }
}
