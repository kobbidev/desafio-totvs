﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Totvs.Presentation.API.ViewModels
{
    public class MensagemDeErro
    {
        public MensagemDeErro(string mensagem)
        {
            this.mensagem = mensagem;
        }
        public string mensagem { get; private set; }
    }
}
