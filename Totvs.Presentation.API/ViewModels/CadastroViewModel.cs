﻿using System;
using System.Collections.Generic;

namespace Totvs.Presentation.API.ViewModels
{
    public class CadastroViewModel
    {
        public string Id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public ICollection<TelefoneViewModel> phones { get; set; }
        public DateTime? created { get; set; }
        public DateTime? modified { get; set; }
        public DateTime? last_login { get; set; }
        public string token { get; set; }
    }

    public class TelefoneViewModel
    {
        public string ddd { get; set; }
        public string number { get; set; }
    }
}