﻿namespace Totvs.Presentation.API.ViewModels
{
    public class LoginViewModel
    {
        public string name { get; set; }
        public string email { get; set; }

        // #Req: Caso o e-mail e a senha correspondam a um usuário existente, retornar igual ao endpoint de Criação.
        // O endpoint de criação tem a propriedade senha, mas não é seguro devolver a senha no json
        // public string password { get; set; }
        public string phones { get; set; }

        // O endpoint de criação não contém token, mas o usuário precisará dele para fazer a requisição do perfil
        public string token { get; set; }
    }
}