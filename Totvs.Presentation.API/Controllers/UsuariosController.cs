﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Totvs.Infrastructure.Database;
using Totvs.Application.UseCases;
using Totvs.Application.Inputs;
using Totvs.Application.Outputs;
using Totvs.Presentation.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using Totvs.Core.Domain;
using AutoMapper;
using Totvs.Presentation.API.ViewModels;

namespace Totvs.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly IUsuariosUseCases _useCases;
        private readonly IMapper _mapper;

        public UsuariosController(DataContext dataContext, IQueryContext queryContext, SignInManager<Usuario> signInManager, IMapper mapper)
        {
            _useCases = new UsuariosUseCases(new CQRS(dataContext, queryContext), signInManager);
            _mapper = mapper;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] LoginDeUsuarioInput login)
        {
            object jsonResponse = null;
            var usecaseOutput = _useCases.Login(login);
            if (usecaseOutput.Status == Output.StatusOutput.Sucesso)
            {
                jsonResponse = _mapper.Map<LoginViewModel>(usecaseOutput);
            }
            else
            {
                jsonResponse = MensagemDeErroFactory.Create(usecaseOutput.Mensagem);
            }

            return this.CreateResult(usecaseOutput, jsonResponse);
        }

        [HttpGet]
        [Route("perfil/{id}")]
        public IActionResult Perfil([FromRoute] int id)
        {
            Object jsonResponse = null;

            var usecaseOutput = _useCases.ConsultarUsuario(id, HttpContext.Request.Headers["token"]);
            if (usecaseOutput.Status == Output.StatusOutput.Sucesso)
            {
                jsonResponse = _mapper.Map<PerfilViewModel>(usecaseOutput);
            }
            else
            {
                jsonResponse = MensagemDeErroFactory.Create(usecaseOutput.Mensagem);
            }
            
            return this.CreateResult(usecaseOutput, jsonResponse);
        }

        [HttpPost]
        public IActionResult Post([FromBody] CadastroUsuarioInput cadastro)
        {
            Object jsonResponse = null;

            var usecaseOutput = _useCases.CadastrarUsuario(cadastro);
            if (usecaseOutput.Status == Output.StatusOutput.Criado)
            {
                jsonResponse = _mapper.Map<CadastroViewModel>(usecaseOutput);
            }
            else
            {
                jsonResponse = MensagemDeErroFactory.Create(usecaseOutput.Mensagem);
            }

            return this.CreateResult(usecaseOutput, jsonResponse);

        }
    }
}
