﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Totvs.Core.Domain
{
    public class Entidade
    {
        public int CriadoPorId { get; set; }
        public DateTime CriadoEm { get; set; }
        public int? AtualizadoPorId { get; set; }
        public DateTime? AtualizadoEm { get; set; }

        public virtual Usuario CriadoPor { get; set; }
        public virtual Usuario AtualizadoPor { get; set; }
    }
}
