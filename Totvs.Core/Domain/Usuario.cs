﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Totvs.Core.Domain
{
    public class Usuario : UsuarioBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo { get; set; }
        public DateTime? LastLogin { get; set; }
        public string Token { get; set; }
    }
}
