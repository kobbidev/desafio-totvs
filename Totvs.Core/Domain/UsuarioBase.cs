﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Totvs.Core.Domain
{
    public abstract class UsuarioBase : IdentityUser
    {
        public string CriadoPorId { get; set; }
        public DateTime? CriadoEm { get; set; }
        public int AtualizadoPorId { get; set; }
        public DateTime? AtualizadoEm { get; set; }
        public string name { get; set; }
        // public string email { get; set; }
        public ICollection<Telefone> Phones { get; set; }
    }
}
