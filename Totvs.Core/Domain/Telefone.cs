﻿using System.ComponentModel.DataAnnotations;

namespace Totvs.Core.Domain
{
    public class Telefone
    {
        [Key]
        public int Id { get; set; }
        public string number { get; set; }
        public string ddd { get; set; }
        public string UsuarioId { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}